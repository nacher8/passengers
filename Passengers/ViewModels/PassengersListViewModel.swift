//
//  PassengersListViewModel.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import SwiftUI
import Combine

public class PassengersListViewModel: ObservableObject {
    //MARK: - Properties
    private var service = PassengersService()
    
    @Published internal var passengersList: [PassengersUserModel]
    
    private var subscriptions = Set<AnyCancellable>()
    
    @Published internal var currentPage: Int = 0
    
    private var totalPages: Int = 0
    
    //MARK: - Init
    init() {
        passengersList = []
        self.getPassengerList(currenPage: currentPage)
    }
    
    //MARK: - Functions
    private func getPassengerList(currenPage: Int) {
        UIApplication.shared.showLoading()
        let result = service.getPassengers(page: currentPage)
        result.sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                UIApplication.shared.hideLoading()
                print(completion)
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }, receiveValue: { value in
            self.totalPages = value.totalPages
            self.passengersList.append(contentsOf: value.data)
        }).store(in: &subscriptions)
    }
    
    public func loadMorePassengers(currentPassenger: PassengersUserModel? = nil) {
        guard let passenger = currentPassenger else {
            return
        }
        
        guard let lastItem = passengersList.last else {
            return
        }
        
        if passenger == lastItem {
            currentPage += 1
            if currentPage < totalPages {
                getPassengerList(currenPage: currentPage)
            }
        }
    }
    
}
