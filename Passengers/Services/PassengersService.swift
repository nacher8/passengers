//
//  PassengersService.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import SwiftUI
import Alamofire
import Combine

public class PassengersService {
    //MARK: - Properties
    private var urlPassengers: String = "https://api.instantwebtools.net/v1/passenger"
    
    //MARK: - Functions
    public func getPassengers(page: Int) -> Future<PassengersResultModel, Error> {
        let parameters = ["page": page,
                          "size": 30]
        return Future<PassengersResultModel, Error> { promise in
            AF.request(self.urlPassengers, method: .get, parameters: parameters)
                .responseDecodable(of: PassengersResultModel.self) { response in
                    switch response.result {
                    case .success(let passengersObj):
                        promise(.success(passengersObj))
                        break
                    case .failure(let error):
                        promise(.failure(error))
                        break
                    }
                }
        }
    }
}
