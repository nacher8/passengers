//
//  UIApplicationDelegate+Loading.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import UIKit
import NVActivityIndicatorView

public extension UIApplication {
    ///Show loading
    @objc func showLoading() {
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                NVActivityIndicatorPresenter.sharedInstance
                    .startAnimating(.init(size: .init(width: 50, height: 50),
                                          message: nil,
                                          messageFont: nil,
                                          messageSpacing: nil,
                                          type: .circleStrokeSpin,
                                          color: .label,
                                          padding: nil,
                                          displayTimeThreshold: 1,
                                          minimumDisplayTime: nil,
                                          backgroundColor: UIColor.systemBackground.withAlphaComponent(0.5),
                                          textColor: nil))
            }
        }
    }
    
    /// Hides loading
    @objc func hideLoading() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
}
