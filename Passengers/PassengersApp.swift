//
//  PassengersApp.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import SwiftUI

@main
struct PassengersApp: App {
    var body: some Scene {
        WindowGroup {
            PassengersListView()
        }
    }
}
