//
//  PassengersUserModel.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import Foundation

public struct PassengersUserModel: Decodable, Hashable {
    var _id: String
    var name: String
    var trips: Int
    var airline: [PassengersAirplaneModel]
    var __v: Int
}

public struct PassengersAirplaneModel: Decodable, Hashable {
    var id: Int
    var name: String
    var country: String
    var logo: String
    var slogan: String
    var head_quaters: String
    var website: String
    var established: String
}
