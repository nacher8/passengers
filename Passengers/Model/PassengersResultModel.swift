//
//  PassengersResultModel.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import Foundation

public struct PassengersResultModel: Decodable {
    var totalPassengers: Int
    var totalPages: Int
    var data: [PassengersUserModel]
}
