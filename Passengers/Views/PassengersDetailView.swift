//
//  PassengersDetailView.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import Foundation
import SwiftUI

public struct PassengersDetailView: View {
    //MARK: - Properties
    private var passenger: PassengersUserModel
    
    public init(passenger: PassengersUserModel) {
        self.passenger = passenger
    }
    
    public var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("INFO PASSENGER")
                .fontWeight(.bold)
                .font(.title)
                .padding(.bottom, 16)
            
            PassengersStatsRow(title: "Name: ", value: passenger.name)
            
            PassengersStatsRow(title: "Trips: ", value: String(passenger.trips))
            
            Text("INFO AIRLINE")
                .fontWeight(.bold)
                .font(.title)
                .padding(.vertical, 16)
            
            PassengersStatsRow(title: "Name: ", value: passenger.airline[0].name)
            
            PassengersStatsRow(title: "Country: ", value: passenger.airline[0].country)
            
            PassengersStatsRow(title: "Slogan: ", value: passenger.airline[0].slogan)
            
            Spacer()
            
        }
        .padding(.leading, 8)
        .padding(.top, 8)
    }
}
