//
//  PassengersStatsRow.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import SwiftUI

struct PassengersStatsRow: View {
    //MARK: - Properties
    private var title: String
    private var value: String
    
    public init(title: String,
                value: String) {
        self.title = title
        self.value = value
    }
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text(title)
                .fontWeight(.bold)
            Text(value)
            Spacer()
        }
    }
}

