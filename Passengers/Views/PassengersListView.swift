//
//  ContentView.swift
//  Passengers
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 11/12/21.
//

import SwiftUI

struct PassengersListView: View {
    //MARK: - Properties
    @StateObject var viewModel = PassengersListViewModel()
    
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.passengersList, id: \.self) { passenger in
                    NavigationLink(destination: PassengersDetailView(passenger: passenger)) {
                        PassengersStatsRow(title: "Name: ", value: passenger.name)
                            .onAppear{
                                viewModel.loadMorePassengers(currentPassenger: passenger)
                            }
                           
                    }
                    .listRowBackground(Color.clear)
                }
            }
            .navigationTitle("Passengers List")
        }
        
    }
}
